import React, {Component, useState, useEffect }  from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import UseHookLevelTemperature from './components/useHookLevelTemperature'
import UseHookPluviometer from './components/useHookPluviometer'
import UseHookGround from './components/useHookGround'

export default function App() {
  
  const [getFetch, setGetFetch] = useState({allData : [], bool: false})
  const [boolTimer, setBoolTimer] = useState(false)

  const timerFetch = () =>{
    setInterval(() => {
      fetchServer()
    }, 5000);
  } 

  const fetchServer = () => {
    fetch('http://127.0.0.1:9000/api/information')
    .then(res => res.json())
    .then(data => {
      //console.log(data)
      let dataFinal = []
      data.devices.map((device) =>{
        //FILTRO DE INFORMACION PARA ORGANIZAR DE ACUERDO AL BOARD-ID DE FORMA ASC
        data.data.map((dataAll, index) => {
          let json = JSON.parse(dataAll.data)
          //console.log(json)
          if(device === json.device.board_id){
            dataFinal.push(dataAll)
          }
        })
      })
      console.log(dataFinal)
      setGetFetch({allData : dataFinal, bool : true})
    })
  }

  const showGround = (date, json) => {
    let con = {
      date:date, board_id:json.board_id, device:json.device_name, 
      accXRaw:json.sensors['1-Acc-X'].RAW, accXEng:json.sensors['1-Acc-X'].ENG, 
      accYRaw:json.sensors['2-Acc-Y'].RAW, accYEng:json.sensors['2-Acc-Y'].ENG, 
      accZRaw:json.sensors['3-Acc-Z'].RAW, accZEng:json.sensors['3-Acc-Z'].ENG,
      humidtyRaw:json.sensors['4-humidity'].RAW, humidtyEng:json.sensors['4-humidity'].ENG, 
      vibrationXRaw:json.sensors['5-Vibration-X'].RAW, vibrationXEng:json.sensors['5-Vibration-X'].ENG, 
      vibrationYRaw:json.sensors['6-Vibration-Y'].RAW, vibrationYEng:json.sensors['6-Vibration-Y'].ENG, 
      vibrationZRaw:json.sensors['7-Vibration-Z'].RAW, vibrationZEng:json.sensors['7-Vibration-Z'].ENG
    }

    return <UseHookGround params = {con} />
  } 

  const showLevelTemperature = (date, json) =>{
    let keys = Object.keys(json.sensors)

    let con = {
      date:date, board_id:json.board_id, device:json.device_name, 
      levelRaw:json.sensors['1-temperature'].RAW, levelEng:json.sensors['1-temperature'].ENG, 
      tempeRaw:json.sensors['2-level'].RAW, tempeEng:json.sensors['2-level'].ENG, 
    }

    if(keys.length>2){
      Object.assign(con,{
        thresholdVibrationRaw:json.sensors['3-threshold-vibration'].RAW, thresholdVibrationEng:json.sensors['3-threshold-vibration'].RAW,
        thresholdSoundRaw:json.sensors['4-threshold-sound'].RAW, thresholdSoundEng:json.sensors['4-threshold-sound'].ENG,
      })
    }

    return <UseHookLevelTemperature params = {con} />
  }

  const showPluviometer = (date, json) =>{
    let keys = Object.keys(json.sensors)
    
    let con = {
      date:date, board_id:json.board_id, device:json.device_name, 
      precipitationRaw:json.sensors['1-precipitation'].RAW, precipitationEng:json.sensors['1-precipitation'].ENG, 
      accumulatedRaw:json.sensors['2-accumulated-precipitation'].RAW, accumulatedEng:json.sensors['2-accumulated-precipitation'].ENG,
      
    }

    if(keys.length>2){
      Object.assign(con,{
        humidityRaw:json.sensors['3-humidity'].RAW, humidityEng:json.sensors['3-humidity'].RAW,
        temperatureRaw:json.sensors['4-temperature'].RAW, temperatureEng:json.sensors['4-temperature'].ENG,
        atmosfericRaw:json.sensors['5-atmosferic-presure'].RAW, atmosfericEng:json.sensors['5-atmosferic-presure'].ENG,
      })
    }

    return <UseHookPluviometer params = {con} />
  }

  useEffect(() => {
    if(!boolTimer){
      timerFetch()
      setBoolTimer(true)
    }
    return () => {
      clearInterval(timerFetch)

    }
  }, [boolTimer])

  return (
    <div>
    <React.Fragment>
      <CssBaseline />
        <Container fixed>
          {boolTimer && getFetch.bool && (
            <div>
            {getFetch.allData.map((get, index) => (
              <div key={index}>
                {JSON.parse(get.data).device.device_name === "ground-threshold" && (
                  <>
                    <div key={index+10}>
                      {
                        showGround(get.date,JSON.parse(get.data).device)
                        // = {device} &&
                        //console.log(JSON.parse(get.data).device) &&
                        
                      }
                    </div>
                    <br />
                  </>
                    )}
                {JSON.parse(get.data).device.device_name === "pluviometer" &&(
                  <>
                    <div key={index+11}>
                      {
                        showPluviometer(get.date,JSON.parse(get.data).device)
                        // = {device} &&
                        //console.log(JSON.parse(get.data).device) &&
                        
                      }
                    </div>
                    <br />
                  </>
                    )}
                {JSON.parse(get.data).device.device_name === "level-temperature" &&(
                  <>
                    <div key={index+12}>
                      {
                        showLevelTemperature(get.date,JSON.parse(get.data).device)
                        // = {device} &&
                        //console.log(JSON.parse(get.data).device) &&
                        
                      }
                    </div>
                    <br />
                  </>
                    )}
              </div>
            ))}
            </div>
          )}
        </Container>
      </React.Fragment>
    </div>
    
  )  
}
