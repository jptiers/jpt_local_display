import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import ListRawEng from './ListRawEng'

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function UseHookGround(...params) {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          <b>Date time: {params[0].params.date}</b>
        </Typography>
        <Typography variant="h5" component="h2">
          Device: {params[0].params.device}
        </Typography>
        <Typography variant="h5" component="h2">
          Board ID: {params[0].params.board_id}
        </Typography>
        <hr></hr>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          <b>Sensors</b>
        </Typography>
        <Typography variant="body2" component="p">
          1-ACC-X:
          {ListRawEng(params[0].params.accXRaw, params[0].params.accXEng)}
        </Typography>
        <Typography variant="body2" component="p">
          2-ACC-Y: 
          {ListRawEng(params[0].params.accYRaw, params[0].params.accYEng)}
        </Typography>
        <Typography variant="body2" component="p">
          3-ACC-Z: 
          {ListRawEng(params[0].params.accZRaw, params[0].params.accZEng)}
        </Typography>
        <Typography variant="body2" component="p">
          4-Humidity: 
          {ListRawEng(params[0].params.humidtyRaw, params[0].params.humidtyEng)}
        </Typography>
        <Typography variant="body2" component="p">
          5-Vibration-X:
          {ListRawEng(params[0].params.vibrationXRaw, params[0].params.vibrationXEng)}
        </Typography>
        <Typography variant="body2" component="p">
          6-Vibration-Y:
          {ListRawEng(params[0].params.vibrationYRaw, params[0].params.vibrationYEng)}
        </Typography>
        <Typography variant="body2" component="p">
          7-Vibration-Z:
          {ListRawEng(params[0].params.vibrationZRaw, params[0].params.vibrationZEng)}
        </Typography>
      </CardContent>
      {/* 
      <CardActions>
        <Button size="small">Learn More</Button>
      </CardActions>
      */}
    </Card>
  );
}
