import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import GrainIcon from '@material-ui/icons/Grain';
import Filter9Icon from '@material-ui/icons/Filter9';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

export default function ListRawEng(raw, eng) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <List component="nav" aria-label="main mailbox folders">
        <ListItem button>
          <ListItemIcon>
            <GrainIcon />
          </ListItemIcon>
          <ListItemText primary={"RAW: "+ raw} />
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <Filter9Icon />
          </ListItemIcon>
          <ListItemText primary={"ENG: "+ eng} />
        </ListItem>
      </List>
      <Divider />
      {/* 
      <List component="nav" aria-label="secondary mailbox folders">
        <ListItem button>
          <ListItemText primary="Trash" />
        </ListItem>
        <ListItemLink href="#simple-list">
          <ListItemText primary="Spam" />
        </ListItemLink>
      </List>
      */}
    </div>
  );
}