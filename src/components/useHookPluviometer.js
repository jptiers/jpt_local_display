import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import ListRawEng from './ListRawEng'

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function UseHookPluviometer(...params) {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          <b>Date time: {params[0].params.date}</b>
        </Typography>
        <Typography variant="h5" component="h2">
          Device: {params[0].params.device}
        </Typography>
        <Typography variant="h5" component="h2">
          Board ID: {params[0].params.board_id}
        </Typography>
        <hr></hr>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          <b>Sensors</b>
        </Typography>
        <Typography variant="body2" component="p">
          1-Precipitation: 
          {ListRawEng(params[0].params.precipitationRaw, params[0].params.precipitationEng)}
        </Typography>
        <Typography variant="body2" component="p">
          2-Acculated Precipitation: 
          {ListRawEng(params[0].params.accumulatedRaw, params[0].params.accumulatedEng)}
        </Typography>
        {
          params[0].params.humidityRaw !== '' && params[0].params.humidityEng &&(
            <Typography variant="body2" component="p">
              3-Humidity: 
              {ListRawEng(params[0].params.humidityRaw, params[0].params.humidityEng)}
            </Typography>
          )
        }
        {
          params[0].params.temperatureRaw !== '' && params[0].params.temperatureEng &&(
            <Typography variant="body2" component="p">
              4-Temperature: 
              {ListRawEng(params[0].params.temperatureRaw, params[0].params.temperatureEng)}
            </Typography>
          )
        }
        {
          params[0].params.atmosfericRaw !== '' && params[0].params.atmosfericEng &&(
            <Typography variant="body2" component="p">
              5-Atmosferic Presure: 
              {ListRawEng(params[0].params.atmosfericRaw, params[0].params.atmosfericEng)}
            </Typography>
          )
        }
      </CardContent>
      {/* 
      <CardActions>
        <Button size="small">Learn More</Button>
      </CardActions>
      */}
    </Card>
  );
}
