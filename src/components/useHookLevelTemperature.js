import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import ListRawEng from './ListRawEng'

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function UseHookLevelTemperature(...params) {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          <b>Date time: {params[0].params.date}</b>
        </Typography>
        <Typography variant="h5" component="h2">
          Device: {params[0].params.device}
        </Typography>
        <Typography variant="h5" component="h2">
          Board ID: {params[0].params.board_id}
        </Typography>
        <hr></hr>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          <b>Sensors</b>
        </Typography>
        <Typography variant="body2" component="p">
          1-Level:
          {ListRawEng(params[0].params.tempeRaw, params[0].params.tempeEng)}
        </Typography>
        <Typography variant="body2" component="p">
          2-Temperature:
          {ListRawEng(params[0].params.levelRaw, params[0].params.levelEng)}
        </Typography>
        {
          params[0].params.thresholdVibrationRaw !== '' && params[0].params.thresholdVibrationEng &&(
            <Typography variant="body2" component="p">
              3-Threshold Vibration: 
              {ListRawEng(params[0].params.thresholdVibrationRaw, params[0].params.thresholdVibrationEng)}
            </Typography>
          )
        }
        {
          params[0].params.thresholdSoundRaw !== '' && params[0].params.thresholdSoundEng &&(
            <Typography variant="body2" component="p">
              4-Threshold Sound: 
              {ListRawEng(params[0].params.thresholdSoundRaw, params[0].params.thresholdSoundEng)}
            </Typography>
          )
        }
      </CardContent>
      {/* 
      <CardActions>
        <Button size="small">Learn More</Button>
      </CardActions>
      */}
    </Card>
  );
}
