const express = require("express");
const app = express();
const sqlite3 = require('sqlite3').verbose();


//conexion en caso de que sea linux, modificar el script
const conLinux = '/jpt/jptmonitoring/External/jptdatabase.db'
//en caso contrario con windows
const conWindows = 'C:/JPT/api/jptdatabase.db'

//se crea la variable la cual soporta la conexion con SQLITE, para prevenir
//se hace la instancia de solo lectura
let db = new sqlite3.Database(conLinux, sqlite3.OPEN_READONLY, (err) => {
    if(err){
        return console.error(err.message)
    }
    console.log("Connect database succesfully!")
})

var response = [{}]
var devices = [];
var uniqueDevices = null;

//ahora se realiza la consulta a la base de datos la cual nos trae los valores de los dispositivos
function addArray(values){
    //console.log(values)
    values.map((device, index) => {
        db.get("SELECT m_datetime as date, m_data as data FROM Nodex"
            + " WHERE m_device= '" + device + "' ORDER BY date DESC LIMIT 1", (err, row) => {
            //let index = {}
            //index[device] = row
            //console.log(index)
            response.push(row)
        })
    })
    //declaramos una variable temporal que nos guardara la respuesta acumuluda
    var retur = response
    //luego limpiamos la variable acumulativa
    //console.log(sortData(response,'asc'))
    response = []
    //console.log(retur)
    return retur
}

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/api/information', function (req, res) {
    //declaracion de la hora para la consulta
    let date_ob = new Date();
    // current date
    // adjust 0 before single digit date
    let date = ("0" + date_ob.getDate()).slice(-2);
    // current month
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    // current year
    let year = date_ob.getFullYear();
    // current hours
    let hours = date_ob.getHours();
    // current minutes
    let minutes = date_ob.getMinutes();

    let initDate = year + "/" + month + "/" + date + " " + hours + ":00:00"
    let endDate = year + "/" + month + "/" + date + " " + hours + ":59:59"

    console.log(initDate + " " + endDate);

    let query = "SELECT m_device as device FROM Nodex"
    + " WHERE m_datetime BETWEEN '" + initDate +" ' AND '" + endDate +" '"

    let test = "SELECT m_device as device FROM Nodex"
    + " WHERE m_datetime BETWEEN '2020/03/27 07:00:00 ' AND '2020/03/27 08:00:00 '"
    //Primero se consulta todos los dispositivos conectados en esa hora para saber que tenemos
    db.all(query, (err, row) => {
        if (err) {
         console.error(err.message);
        }
        
        row.map((device, index)=>{
            devices.push(device.device)
        })

        uniqueDevices = devices.filter((value, index, self) =>{
            return self.indexOf(value) === index
        })
        
        //ahora se recorre el id unico de los dispositivos y se comienza a realizar las consultas necesarias para cada uno
        //uniqueDevices

        res.json({"data" : addArray(uniqueDevices.sort()) , "devices" : uniqueDevices.sort()})
    })
})

app.listen(9000, () => {
    console.log("El servidor est� inicializado en el puerto 9000");
});
